@extends('layouts.app')
@section('title', 'Edit candidate')
@section('content')
    <h1> Edit candidate </h1>
    <form method = "post" action = "{{action('CandidatesController@update',$candidate->id)}}">
    @csrf
    @method('PATCH')
    <div class="form-group">
        <label for = "name">Candidate name</label>
        <input type = "text"  class="form-control-sm" name = "name" value = {{$candidate->name}}>
    </div>
    <div class="form-group">
        <label for = "email">Candidate email</label>
        <input type = "text"  class="form-control-sm" name = "email" value = {{$candidate->email}}>
    </div>
    <div class="form-group">
        <input type = "submit"  class="btn btn-outline-dark btn-lg" name = "submit" value = "update candidate">
    </div>
    </form>
@endsection