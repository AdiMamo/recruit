@extends('layouts.app')
@section('title','Create candidate')
@section('content')
    <h1> Create candidate </h1>
    <form method = "post" action = "{{action('CandidatesController@store')}}">
    @csrf
    <div class="form-group" >
        <label for = "name">Candidate name</label>
        <input type = "text" class="form-control-sm" name = "name" placeholder="input your name" >
    </div>
    <div class="form-group">
        <label for = "email">Candidate email</label>
        <input type = "text" class="form-control-sm" name = "email" placeholder="input your email">
    </div>
    <div class="form-group">
        <input type = "submit" class="btn btn-outline-dark btn-lg" name = "submit" value = "Create candidate">
    </div>

    </form>
@endsection